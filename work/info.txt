station.csv : Contient des données qui représentent une station où les utilisateurs peuvent récupérer ou rendre des vélos.
status.csv : données sur le nombre de vélos et de quais disponibles pour une station et une minute données.
trips.csv : Données sur les voyages à vélo individuels
weather.csv : Données sur la météo d'un jour spécifique pour certains codes postaux